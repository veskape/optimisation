function [GradientCoutb] =GradientCoutb(a,b,dimension,x,y,sigma)
GradientCoutb=zeros(dimension,dimension);
for k=1:dimension
    for j=1:dimension
        sb=0;
        for i=1:30
            sb=sb+(2*(a(k)*x(i)+b(j)-y(i)))/(sigma*sigma+(a(k)*x(i)+b(j)-y(i))*(a(k)*x(i)+b(j)-y(i)));
        end
        GradientCoutb(k,j)=0.5*sb;
    end
end
