function [alpha] = FletcherLeMarechalCoutRobuste(alpha,dk,AB,GradCR,B1,B2,lambda,x,y,sigma)
cw1=false;
cw2=false;
alphad=inf;
alphag=0;
while (cw1==false || cw2==false)
    if CoutRobusteAB(AB(1)+alpha*dk(1),AB(2)+alpha*dk(2),x,y,sigma) <= CoutRobusteAB(AB(1),AB(2),x,y,sigma)-alpha*(-B1*GradCR'*dk)  %CW1
        cw1=true;
    else
        cw1=false;
        alphad=alpha;
        alpha=(alphag+alphad)/2;
    end
    if cw1==true
        if ([AGradient(sigma,AB(1)+alpha*dk(1),AB(2)+alpha*dk(2),x,y);BGradient(sigma,AB(1)+alpha*dk(1),AB(2)+alpha*dk(2),x,y)]'*dk)/(GradCR'*dk) <= B2  %CW2
            cw2=true;
        else
            cw2=false;
            alphag=alpha;
            if (alphad<inf)
                alpha=(alphad+alphag)/2;
            else 
                alpha=lambda*alpha;
            end
        end
    end
end