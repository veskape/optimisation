function [alpha] = FletcherLeMarechalMoindresCarres(alpha,dk,Tempo2,AB,Gradf,B1,B2,lambda,x,y)
cw1=false;
cw2=false;
alphad=inf;
alphag=0;
while (cw1==false || cw2==false)
    if (norm(Tempo2*AB-y))^2-alpha*(-B1*Gradf'*dk) >= (norm(Tempo2*(AB+alpha.*dk)-y))^2  %CW1
        cw1=true;
    else
        cw1=false;
        alphad=alpha;
        alpha=(alphag+alphad)/2;
    end
    if cw1==true
        if transpose((2*transpose(Tempo2)*(Tempo2*(AB+alpha.*dk)-y)))*dk./transpose(Gradf)*dk <= B2  %CW2
            cw2=true;
        else
            cw2=false;
            alphag=alpha;
            if (alphad<inf)
                alpha=(alphad+alphag)/2;
            else 
                alpha=lambda*alpha;
            end
        end
    end
end