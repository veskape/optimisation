function [callback] = MoindresCarres(a, b, x, y)
  % Initialisation
  callback = 0;
  N = size(x, 1);
  % On applique la m�thode des moindres carr�es pour chaque valeur notre intervalle
  % a et b sur les �l�ments x et y. 
  for i=1:N
      callback = callback + (a*x(i) + b - y(i))^2;
  end
end